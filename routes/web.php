<?php

use App\Http\Controllers\CloudMessagingController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'notif','middleware' => ['auth']] , function (){
    Route::get('/', [CloudMessagingController::class, 'index'])->name('cloud-messaging');
    Route::post('/save-token', [CloudMessagingController::class, 'saveToken'])->name('save-token');
    Route::post('/send-notification', [CloudMessagingController::class, 'sendNotification'])->name('send-notification');
    Route::post('/send-notification-curl', [CloudMessagingController::class, 'sendNotificationByCurl'])->name('send-notification-curl');
});


