<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CloudMessagingController extends Controller
{
    
    /**
     * Show notification page
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('cloud-messaging');
    }

    /**
     * save fcm token when user allow
     *
     * @param  Request $request
     * @return response
     */
    public function saveToken(Request $request){
        $request->user()->update(['fcm_token'=>$request->token]);
        return response()->json(['token saved successfully.']);   
    }
    
    /**
     * send notification
     *
     * @param  Request $request
     * @return void
     */
    public function sendNotification(Request $request)
    {
        $this->validate($request , [
            'title' => 'required|string|max:50',
            'body' => 'required|string|max:255'
        ]);
        $firebaseToken = User::whereNotNull('fcm_token')->pluck('fcm_token')->all();
        if(empty(array_filter($firebaseToken))){
            return redirect()->route('cloud-messaging');
        }

        $SERVER_API_KEY =  env('SERVER_KEY');
        $headers = [
            'Authorization' => 'key=' . $SERVER_API_KEY,
            'Content-Type' => ' application/json'
        ];

        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,  
            ]
        ];

        $response = Http::withoutVerifying()
        ->withHeaders($headers)
        ->connectTimeout(500)
        ->post('https://fcm.googleapis.com/fcm/send',$data);
        $results = $response->getBody();
        $fields = json_decode($results);
      
        return redirect()->route('cloud-messaging');
    }

    /**
     * send notification by curl
     *
     * @param  Request $request
     * @return void
     */
    public function sendNotificationByCurl(Request $request)
    {

        $this->validate($request , [
            'title' => 'required|string|max:50',
            'body' => 'required|string|max:255'
        ]);
        $firebaseToken = User::whereNotNull('fcm_token')->pluck('fcm_token')->all();

        $data = [
            "registration_ids" => $firebaseToken,
            "notification" => [
                "title" => $request->title,
                "body" => $request->body,  
            ]
        ];
        $dataString = json_encode($data);

        $SERVER_API_KEY =  env('SERVER_KEY');
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);
  
        return redirect()->route('cloud-messaging');
    }
}
